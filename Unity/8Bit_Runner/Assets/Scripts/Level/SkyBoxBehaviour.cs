﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class SkyBoxBehaviour : MonoBehaviour
    {
        public Camera mainCamera;
        private Vector3 m_position = new Vector3(0.0f, 0.0f, 0.0f);

        // Use this for initialization
        void Start()
        {
            mainCamera = CameraHelper.FPSCamera;
        }

        // Update is called once per frame
        void Update()
        {
            m_position.y = transform.transform.position.y;
            m_position.z = mainCamera.transform.position.z + 20;
            transform.position = m_position;
        }
    }
}
