﻿// the sky plane always "looks" at the player and stays at a given distance
// the uv corrdinates are relative to the camera rotation, so that the scenery "scrolls" when turning

using UnityEngine;

namespace EightBitRunner
{
    public class SkyBehaviour : MonoBehaviour
    {
        private Camera m_fpsCamera;
        private Renderer m_rend;        
        private Vector3 m_cameraAngle = new Vector3(0.0f, 0.0f, 0.0f);
        private Vector2 m_offset = new Vector2(0.0f, 0.0f);

        void Awake()
        {
            // get renderer
            m_rend = GetComponent<Renderer>();

            // set scale
            transform.localScale = new Vector2(300.0f, 100.0f);
        }

        void Start()
        { 
            // get fps camera
            m_fpsCamera = CameraHelper.FPSCamera;
        }

        void Update()
        {            
            // set position in front of camera
            transform.position = m_fpsCamera.transform.position + (m_fpsCamera.transform.forward * GameConstants.Instance.SKYBOX_DISTANCE) + (m_fpsCamera.transform.up * GameConstants.Instance.SKYBOX_Y_OFFSET);

            // set angle to face camera
            m_cameraAngle.y = m_fpsCamera.transform.localEulerAngles.y - 180.0f;
            transform.localEulerAngles = m_cameraAngle;

            // set uv coordinates for "scrolling" when rotating the camera
            m_offset.x = -m_fpsCamera.transform.rotation.y * GameConstants.Instance.SKYBOX_SCROLL_SPEED;
            m_rend.material.SetTextureOffset("_MainTex", m_offset);
        }
    }
}
