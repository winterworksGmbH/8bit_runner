﻿// helper class for creating layerMask variables
// in unity, layerMask contains byte flags for each layer that should be tested

namespace EightBitRunner
{
    static class LayerHelper
    {
        // provide a variable number of layers and return corresponding layermask
        public static int CreateLayer(params LayerMasks[] args)
        {
            int result = 0;

            for (int i = 0; i < args.Length; i++)
                result |= 1 << (int)args[i];

            return result;
        }
    }
}
