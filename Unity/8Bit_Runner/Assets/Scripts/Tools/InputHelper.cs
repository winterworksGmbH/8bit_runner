﻿// helper class to distinquish between mouseinput and touchinput according to platform
using UnityEngine;

public static class InputHelper
{
    public static bool IsPressed()
    {
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
            return Input.GetMouseButton(0);
        else
            return Input.touchCount > 0;
    }

    public static Vector3 GetTouchPos()
    {
        if (Application.platform == RuntimePlatform.WindowsPlayer || Application.platform == RuntimePlatform.WindowsEditor)
            return Input.mousePosition;
        else
            return Input.GetTouch(0).position;
    }
}