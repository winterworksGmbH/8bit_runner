﻿using UnityEngine;

namespace EightBitRunner
{
    public class PlayerInput : MonoBehaviour
    {
        private bool m_inputReleased = false;

        void Update()
        {
            checkTouch();   
        }

        private void checkTouch()
        {
            if (UI_Controller.Instance.UIState != UI_Controller.UISTATES.HUD) return;
            if (InputHelper.IsPressed())
            {
                // input was released or weapon has autofire?
                if (m_inputReleased || Weapon.Instance.HasAutoFire())
                {
                    Weapon.Instance.Shoot();
                    m_inputReleased = false;
                }
            }
            else
                m_inputReleased = true;
        }
    }
}