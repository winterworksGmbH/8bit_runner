﻿// weapon template file
// a template contains the sprite that is used, the animationcontroller and the audiofx
using UnityEngine;
using UnityEngine.UI;

namespace EightBitRunner
{
    [CreateAssetMenu(menuName = "Weapons/WeaponTemplate")]
    public class WeaponTemplate : ScriptableObject
    {        
        public Sprite SpriteWeapon;
        public Sprite Icon;
        public AnimatorOverrideController AnimationController;
        public AudioClip AudioClipFire;
    }
}