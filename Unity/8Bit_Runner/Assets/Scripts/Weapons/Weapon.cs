﻿using System;
using UnityEngine;

namespace EightBitRunner
{
    [RequireComponent(typeof(AudioSource))]

    public class Weapon : Singleton<Weapon>
    {
        private int m_ammo;        
        private WeaponData m_weaponData;
        private Animator m_animator;
        private SpriteRenderer m_spriteRenderer;
        private Camera mainCamera;
        private AudioSource m_audioSource;
        private int layerMask = LayerHelper.CreateLayer(LayerMasks.WALLS, LayerMasks.FLOORS, LayerMasks.CEILINGS, LayerMasks.ENEMY);

        public bool HasAutoFire()
        {
            return m_weaponData.AutoFire;
        }

        private float m_fireDelay;

        public void SetWeapon(WeaponData _weaponData)
        {
            m_weaponData = _weaponData;
            initFromTemplate();
        }

        void Awake()
        {
            m_ammo = 0;
            m_animator = GetComponent<Animator>();
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_audioSource = GetComponent<AudioSource>();
        }

        void Start()
        { 
            mainCamera = CameraHelper.FPSCamera;

            // init default weapon
            SetWeapon(GameData.Instance.WeaponValues[0]);
        }

        void Update()
        {
            if (m_fireDelay > 0.0f)
                m_fireDelay -= Time.deltaTime * 1000.0f;
        }

        public void Shoot()
        {
            // cannot shoot yet?
            if (m_fireDelay > 0.0f)
                return;

            // set new fire delay
            m_fireDelay = m_weaponData.FireRate;

            m_animator.SetTrigger("Shoot");
            GunGlow.Instance.Shoot();            
            m_audioSource.Play();

            checkIfShotHit();            
        }

        private void setPosition()
        {
            transform.position = mainCamera.ViewportToWorldPoint(new Vector3(1, 1, mainCamera.nearClipPlane));
        }

        private void checkIfShotHit()
        {
            RaycastHit hitInfo;
            Ray ray = mainCamera.ScreenPointToRay(Input.mousePosition);
            Physics.SphereCast(ray, GameConstants.TAP_HIT_RADIUS, out hitInfo, GameConstants.Instance.WEAPON_MAX_RAYCAST_DISTANCE, layerMask);

            // hit?
            if (null != hitInfo.collider && null != hitInfo.collider.gameObject)
            {
                switch (hitInfo.collider.tag)
                {
                    case Tags.SOLID:
                        hitSolid(hitInfo);
                        break;

                    case Tags.COLLIDER_HEAD:
                    case Tags.COLLIDER_BODY:
                    case Tags.GAME_OBJECT:
                        hitGameObject(hitInfo);
                        break;
                }
            }
        }

        private void hitGameObject(RaycastHit hitInfo)
        {
            // ATTENTION! game objects have colliders as their CHILDREN, so the gameobject of this collider is its parent!
            GameObjectBase go = hitInfo.collider.transform.parent.gameObject.GetComponent<GameObjectBase>();

            // spawn blood
            Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_PARTICLES + "ParticleBlood"), hitInfo.point, Quaternion.identity);            

            // check for headshot damage
            float multiplier = 1.0f;
            if (hitInfo.collider.tag.Equals(Tags.COLLIDER_HEAD))
                multiplier = m_weaponData.HeadShotMultiplier;

            go.hit(m_weaponData.Damage * multiplier);
        }

        private static void hitSolid(RaycastHit hitInfo)
        {
            // spawn hole and particle effect
            Vector3 startPos = hitInfo.point;
            Quaternion startRot = Quaternion.LookRotation(hitInfo.normal);            
            Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_PARTICLES + "ParticleBulletHole"), startPos, startRot);
            Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_PARTICLES + "ParticleBulletHit"), startPos + hitInfo.normal * 0.025f, Quaternion.identity);
        }        

        private void initFromTemplate()
        {
            m_spriteRenderer.sprite = m_weaponData.Template.SpriteWeapon;
            m_animator.runtimeAnimatorController = m_weaponData.Template.AnimationController;
            m_audioSource.clip = m_weaponData.Template.AudioClipFire;
            m_fireDelay = 0.0f;
        }
    }
}