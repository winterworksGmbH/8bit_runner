﻿using System;
using UnityEngine;

namespace EightBitRunner
{
    public class GameObjectBase : MonoBehaviour
    {
        protected float m_health;
        protected SpriteRenderer m_spriteRenderer;
        protected Animator m_animator;

        protected virtual void Awake()
        {
            m_spriteRenderer = GetComponent<SpriteRenderer>();
            m_animator = GetComponent<Animator>();
        }

        public void hit(float _damage)
        {
            m_health -= _damage;

            if (m_health <= 0.0)
                die();
        }

        virtual protected void die()
        {
            // disable colliders except physics collider
            foreach (Collider c in GetComponentsInChildren<Collider>())
            {
                if (!c.gameObject.CompareTag(Tags.COLLIDER_PHYSICS))
                    c.enabled = false;
            }
        }
    }
}