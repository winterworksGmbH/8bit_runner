﻿// enemy template file

using UnityEngine;

namespace EightBitRunner
{
    [CreateAssetMenu(menuName = "Enemies/EnemyTemplate")]
    public class EnemyTemplate : ScriptableObject
    {
        public Sprite SpriteEnemy;
        public AnimatorOverrideController AnimationController;
    }
}