﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class Enemy : GameObjectBase
    {
        private float m_fireDelay;
        private bool m_canShoot = false;
        private EnemyData m_data;        
        private GameObject m_player;        
        private EnemyState m_state = EnemyState.idle;
        private Dictionary<EnemyState, Action> m_stateFunctionLUT = new Dictionary<EnemyState, Action>();

        protected override void Awake()
        {
            base.Awake();
            m_player = GameObject.FindGameObjectWithTag(Tags.PLAYER);
            SetState(EnemyState.idle);

            m_stateFunctionLUT.Add(EnemyState.idle, RunIdle);
            m_stateFunctionLUT.Add(EnemyState.run, RunRun);
            transform.rotation = Quaternion.identity;
        }
        void Start()
        {
            Init(GameData.Instance.EnemyValues[0]);
        }

        void Update()
        {
            if (m_fireDelay > 0.0)
                m_fireDelay -= Time.deltaTime * 1000;
            else
                m_canShoot = true;

            // run sate function
            if (m_stateFunctionLUT.ContainsKey(m_state))
                m_stateFunctionLUT[m_state].Invoke();
        }
        
        private bool closerToPlayerThan(float _value)
        {
            return Vector3.Distance(transform.position, m_player.transform.position) < _value;
        }

        private bool checkAttack()
        {
            if (closerToPlayerThan(m_data.AttackRange))
            {
                if (m_canShoot)
                {
                    StartAttack();                    
                }

                return true;
            }

            return false;
        }

        private void RunIdle()
        {
            if (!checkAttack())
            {
                if (closerToPlayerThan(m_data.RunRange))
                {
                    StartRun();
                }
            }
        }

        private void StartAttack()
        {
            SetState(EnemyState.attack);
            m_canShoot = false;
            m_fireDelay = m_data.FireRate;
        }

        private void StartRun()
        {
            SetState(EnemyState.run);
        }

        private void RunRun()
        {
            // attack?
            if (!checkAttack())
            { 
                // move towards player
                Vector3 target = new Vector3(m_player.transform.position.x, transform.position.y, m_player.transform.position.z);
                transform.position = Vector3.MoveTowards(transform.position, target, m_data.RunSpeed * Time.deltaTime);
            }
        }

        private void AttackDone()
        {
            SetState(EnemyState.idle);
        }

        public void Init(EnemyData _data)
        {            
            m_data = _data;
            m_spriteRenderer.sprite = m_data.Template.SpriteEnemy;
            m_animator.runtimeAnimatorController = m_data.Template.AnimationController;
            m_health = m_data.Health;
        }

        private void SetState(EnemyState _state)
        {
            m_state = _state;
            //m_animator.SetTrigger(_state.ToString());
            m_animator.Play(_state.ToString(), 0);
        }

        protected override void die()
        {
            base.die();
            SetState(EnemyState.die);
        }
    }
}