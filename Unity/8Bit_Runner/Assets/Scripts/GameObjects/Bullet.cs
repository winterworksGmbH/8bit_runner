﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace EightBitRunner
{
    [RequireComponent(typeof(SpriteRenderer))]
    [RequireComponent(typeof(BoxCollider))]
    [RequireComponent(typeof(AnimatorOverrideController))]
    public class Bullet : MonoBehaviour
    {
        public GameObject ParticleFX;
        public AudioSource SoundFX_Hit;

        private float m_bulletSpeed;
        private float m_bulletRange;
        private int m_bulletDamage;

        void Start()
        {
            InitBullet(transform.position, 5, 5, 155);
        }

        void OnCollisionEnter(Collision _collision)
        {

            if (_collision.collider.CompareTag("Player"))
            {
                Debug.Log("hit player!");
               // _collision.collider.gameObject.GetComponent<Player>().TakeDamage(150);
                Destroy(this.gameObject);
    
        }
        }

        public void InitBullet(Vector3 _origin, float _speed, float _range, int _damage)
        {
            m_bulletSpeed = _speed;
            m_bulletRange = _range;
            m_bulletDamage = _damage;

            float _target = _origin.z - _range;
            transform.DOLocalMoveZ(_target, _speed);
        }
    }
}