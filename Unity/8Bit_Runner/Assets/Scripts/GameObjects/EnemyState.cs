﻿namespace EightBitRunner
{
    public enum EnemyState
    {
        idle,
        run,
        attack,
        die,
        dead
    }
}