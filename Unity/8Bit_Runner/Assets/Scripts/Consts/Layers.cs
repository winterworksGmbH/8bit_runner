﻿// layer lookup helper
namespace EightBitRunner
{
    public enum LayerMasks
    {
        FLOORS = 8,
        CEILINGS = 9,
        WALLS = 10,
        ENEMY = 11,
        PARTICLES = 14
    }

    public static class SortingLayer
    {
        public const string GAME_OBJECTS = "GameObjects";
    }
}
