﻿// global gameconstants go here
using UnityEngine;

namespace EightBitRunner
{
    public class GameConstants : Singleton<GameConstants>
    {        
        // camera
        public float CAMERA_COLLISION_CAPSULE_LENGTH = 1.5f;
        public float CAMERA_COLLISION_CAPSULE_RADIUS = 0.1f;
        public float CAMERA_COLLISION_RAY_LENGTH = 5.0f;
        public float CAMERA_HEAD_POSITION_Y = 1.5f;
        public float CAMERA_HEAD_MOVEMENT_SPEED = 6.0f;
        public float CAMERA_HEAD_MOVEMENT_FACTOR = 0.03f;
        public float CAMERA_MOVEMENT_SPEED = 1.0f;

        // weapon
        public float WEAPON_MAX_RAYCAST_DISTANCE = 40.0f;

        // skybox
        public float SKYBOX_DISTANCE = 140.0f;
        public float SKYBOX_Y_OFFSET = 40.0f;
        public float SKYBOX_SCROLL_SPEED = 2.0f;

        // lights
        public float LIGHT_GUN_GLOW_DIM_SPEED = 1.0f;

        // resource paths
        public string RESOURCE_PATH_LEVEL = "Prefabs/Level/";
        public string RESOURCE_PATH_PARTICLES = "Prefabs/Particles/";
        public string RESOURCE_PATH_WEAPON_TEMPLATES = "Templates/WeaponTemplates/";
        public string RESOURCE_PATH_ENEMY_TEMPLATES = "Templates/EnemyTemplates/";
        public string RESOURCE_PATH_ENEMY = "Prefabs/Enemies/";
        public string RESOURCE_PATH_UI = "Prefabs/UI/";

        // urls
        public string URL_BALANCING_DATA = "https://docs.google.com/spreadsheets/d/1Qg5BNvem-qjDSlkDtVDeYNZsRMJdqgj9h1n574Vo5ts/edit?usp=sharing";
        public static string SPREAD_SHEET_ID = "1Qg5BNvem-qjDSlkDtVDeYNZsRMJdqgj9h1n574Vo5ts";
        public static string SPREAD_SHEET_AUTH_URL = "https://accounts.google.com/o/oauth2/v2/auth?prompt=consent&response_type=code&client_id=557750179789-6b99jueebsrpkr9q8900cbhh35tls9gf.apps.googleusercontent.com&scope=https://www.googleapis.com/auth/spreadsheets.readonly&access_type=offline&redirect_uri=http://localhost";
        public static string SPREAD_SHEET_GET = "https://sheets.googleapis.com/v4/spreadsheets/1Qg5BNvem-qjDSlkDtVDeYNZsRMJdqgj9h1n574Vo5ts?includeGridData=true&alt=json&key=JrlSnQFvQPnxWYHgZOyXZHp6";
        public static float TAP_HIT_RADIUS = 0.1f;
    }
}
