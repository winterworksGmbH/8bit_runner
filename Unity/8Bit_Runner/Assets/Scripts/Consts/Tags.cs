﻿// tag lookup helper
namespace EightBitRunner
{
    public class Tags
    {
        public const string FPS_CAMERA = "fps_camera";
        public const string GUN_GLOW_LIGHT = "gun_glow_light";
        public const string SOLID = "solid";
        public const string GAME_OBJECT = "game_object";
        public const string COLLIDER_BODY = "collider_body";
        public const string COLLIDER_HEAD = "collider_head";
        public const string COLLIDER_PHYSICS = "collider_physics";
        public const string PLAYER = "Player";
    }
}
