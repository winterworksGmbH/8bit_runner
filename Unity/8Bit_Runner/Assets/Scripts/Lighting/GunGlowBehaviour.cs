﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class GunGlowBehaviour : MonoBehaviour
    {
        private float m_intensity = 0.0f;
        private Light lt;
        
        void Awake()
        {
            // save starting intensity
            lt = GetComponent<Light>();
            m_intensity = lt.intensity;
            lt.intensity = 0.0f;
        }
        
        void Update()
        {
            // already dark?
            if (lt.intensity == 0.0f)
                return;

            // dim light            
            lt.intensity -= GameConstants.Instance.LIGHT_GUN_GLOW_DIM_SPEED * Time.deltaTime;

            // lights out?
            if (lt.intensity <= 0.0f)
            {
                lt.intensity = 0.0f;
                gameObject.SetActive(false);
            }
        }

        public void StartGlow()
        {
            lt.intensity = m_intensity;
            gameObject.SetActive(true);
        }
    }
}