﻿using UnityEngine;

namespace EightBitRunner
{
    public class GunGlow : Singleton<GunGlow>
    {
        private GunGlowBehaviour[] m_lights;

        public void GetLightsFromScene()
        {
            // get all gameobjects with tag "gun_glow"
            GameObject[] objects = GameObject.FindGameObjectsWithTag(Tags.GUN_GLOW_LIGHT);
            m_lights = new GunGlowBehaviour[objects.Length];

            // get behaviour script
            for (int i = 0; i < m_lights.Length; i++)
            {
                m_lights[i] = objects[i].GetComponent<GunGlowBehaviour>();
            }
        }

        public void Shoot()
        {
            // no lights?
            if (null == m_lights)
                return;

            // start glow for all lights
            foreach (GunGlowBehaviour b in m_lights)
            {
                b.StartGlow();
            }
        }
    }
}