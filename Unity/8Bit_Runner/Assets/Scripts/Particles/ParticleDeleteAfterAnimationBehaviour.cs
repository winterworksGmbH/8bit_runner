﻿// this script deletes a sprite after its animation was played
// useful for expliosions and stuff
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class ParticleDeleteAfterAnimationBehaviour : MonoBehaviour {

        private Animator m_animator;
        private SpriteRenderer m_spriteRenderer;

        void Awake()
        {
            m_animator = GetComponent<Animator>();
            m_spriteRenderer = GetComponent<SpriteRenderer>();
        }

        void Start()
        { 
            // kill after animation
            AnimationEvent evt = new AnimationEvent();
            evt.time = m_animator.runtimeAnimatorController.animationClips[0].length;
            evt.functionName = "kill";
            m_animator.runtimeAnimatorController.animationClips[0].AddEvent(evt);
        }

        private void kill()
        {
            Destroy(gameObject);
        }
    }
}