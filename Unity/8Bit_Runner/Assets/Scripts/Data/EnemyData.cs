﻿// this class holds blueprint information about a single enemy

using UnityEngine;

namespace EightBitRunner
{
    [System.Serializable]
    public class EnemyData
    {
        public string Id;
        public int Health;
        public float RunSpeed;        
        public float RunRange;
        public float FireRate;
        public float AttackRange;
        public int Gold;
        public EnemyTemplate Template;

        [HideInInspector]
        public string TemplateName;
    }
}
