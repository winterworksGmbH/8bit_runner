﻿// this class holds information about all game items
// it is filled automatically from google spread sheet data

using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{

    public class GameData : Singleton<GameData>
    {
        public List<WeaponData> WeaponValues = new List<WeaponData>();
        public List<EnemyData> EnemyValues = new List<EnemyData>();
    }
}
