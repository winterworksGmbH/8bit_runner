﻿// enum for bullet type

namespace EightBitRunner
{
    public enum WeaponBulletType
    {
        PistolBullet = 0,
        ShotgunShell = 1
    }
}
