﻿// this class holds blueprint information about a single weapon

using UnityEngine;

namespace EightBitRunner
{
    [System.Serializable]
    public class WeaponData
    {
        public string Id;
        public WeaponBulletType BulletType;
        public int Damage;
        public float HeadShotMultiplier;
        public int BulletsPerShot;
        public int BulletDelay;
        public int FireRate;
        public bool AutoFire;
        public int Clipsize;
        public int ReloadTime;
        public int MaxAmmo;
        public WeaponTemplate Template;

        [HideInInspector]
        public string TemplateName;
    }
}
