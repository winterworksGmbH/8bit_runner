﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class UI_AudioController : Singleton<UI_AudioController>
    {
        public AudioClip Explosion;
        public AudioClip Click;
        public AudioClip Upgrade;

        private AudioSource m_Audiosource;

        void Awake()
        {
            m_Audiosource = GetComponent<AudioSource>();
        }   

        public void PlaySound(AudioClip _audioClip)
        {
            m_Audiosource.clip = _audioClip;
            m_Audiosource.Play();
        }
    }
    
}