﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace EightBitRunner
{
    public class UI_Controller : Singleton<UI_Controller>
    {

        public enum UISTATES
        {
            START,
            HUD,
            OPTIONS,     
            NEXTLEVEL
        }

        public GameObject StartScreen;
        public GameObject HUDScreen;
        public GameObject OptionsScreen;
        public GameObject EventSystem;
        

        public GameObject LeftPanel;
        public GameObject RightPanel;
        public GameObject ContinueButton;

        public UISTATES UIState {
            get { return m_uiState; }
            set { m_uiState = value; ChangeUIState(m_uiState); }
        }
        
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private UISTATES  m_uiState;
       
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        void Awake()
        {
            OptionsScreen.SetActive(false);
            HUDScreen.SetActive(false);
            StartScreen.SetActive(true);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public void EnableInput(bool _enabled)
        {
            EventSystem.SetActive(_enabled);
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void ChangeUIState(UISTATES _uiState)
        {
            StartCoroutine(_uiState.ToString() + "State");
        }        
        //--------------------------------------------------------------------------------------------------------------------------------------
        private void TransitionUIScreen(GameObject _screen,bool _isActive)
        {
       
            _screen.SetActive(_isActive);
            
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #region States
        IEnumerator STARTState()
        {
            TransitionUIScreen(StartScreen, true);
            
            while (UIState == UISTATES.START)
            {
                yield return null;
            }
            
            StartScreen.GetComponent<StartScreen_AnimationController>().TransitionToGame(() => TransitionUIScreen(StartScreen,false)); // trigger start screen animations, then hide screen          
        }
        //--------------------------------------------------------------------------------------------------------------------------------------
        IEnumerator HUDState()
        {
            TransitionUIScreen(HUDScreen, true);
            while (UIState == UISTATES.HUD)
            {
                yield return null;
            }
            TransitionUIScreen(HUDScreen, false);
        }
        //--------------------------------------------------------------------------------------------------------------------------------------
        IEnumerator OPTIONSState()
        {

            // Show Screen
            TransitionUIScreen(OptionsScreen, true);
            ContinueButton.GetComponent<ElementMoveTransition>().Transition(TransitionType.TRANSITION_IN, TransitionDirection.BOTTOM, 0.25f);
            LeftPanel.GetComponent<ElementMoveTransition>().Transition(TransitionType.TRANSITION_IN, TransitionDirection.LEFT, 0.25f);
            RightPanel.GetComponent<ElementMoveTransition>().Transition(TransitionType.TRANSITION_IN, TransitionDirection.RIGHT,0.25f,_onStart:()=>EnableInput(false),_callBack: () => EnableInput(true));

            while (UIState == UISTATES.OPTIONS)
            {
                Time.timeScale = 0;
                yield return null;
            }

            // when the state changes
            Time.timeScale = 1;

            // tween panel out, hide panels gets called once the left transition is finished
            ContinueButton.GetComponent<ElementMoveTransition>().Transition(TransitionType.TRANSITION_OUT, TransitionDirection.BOTTOM, 0.25f);
            RightPanel.GetComponent<ElementMoveTransition>().Transition(TransitionType.TRANSITION_OUT, TransitionDirection.RIGHT, 0.25f,_onStart: () => EnableInput(false), _callBack: () => EnableInput(true));
            LeftPanel.GetComponent<ElementMoveTransition>().Transition(TransitionType.TRANSITION_OUT, TransitionDirection.LEFT, 0.25f, _callBack: () => TransitionUIScreen(OptionsScreen, false));
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        #endregion
    }
}