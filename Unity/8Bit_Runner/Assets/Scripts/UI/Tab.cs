﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class Tab : MonoBehaviour
    {
        public GameObject ActiveBG;
        public string TabName;


        public void TabPressed()
        {
            
            // converting the string from the tab name to the enum 
            Tab_Controller.Instance.TabState = (Tab_Controller.TABSTATE)System.Enum.Parse(typeof(Tab_Controller.TABSTATE), TabName);
        }
        public void SetActive(bool _active)
        {
            ActiveBG.SetActive(_active);
        }
        
    }
}