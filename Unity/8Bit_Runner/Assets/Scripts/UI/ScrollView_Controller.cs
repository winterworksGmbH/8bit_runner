﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace EightBitRunner
{
    public class ScrollView_Controller : Singleton<ScrollView_Controller>
    {
        public Transform ScrollViewGrid;

        private void ClearList()
        {
            if (ScrollViewGrid.transform.childCount > 0)
            {
                foreach (Transform _child in ScrollViewGrid.transform)
                {
                    Destroy(_child.gameObject);
                }
            }
        }
        public void InitItems()
        {
            ClearList();
            switch (Tab_Controller.Instance.TabState)
            {
                case Tab_Controller.TABSTATE.SKILLS:
                    Debug.Log("Skill List");
                    break;
                case Tab_Controller.TABSTATE.WEAPONS:
                    InitWeaponItems();
                    break;
                case Tab_Controller.TABSTATE.OPTIONS:
                    Debug.Log("Option List");
                    break;
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void InitWeaponItems()
        {           
            foreach(WeaponData _Data in GameData.Instance.WeaponValues)
            {
                GameObject _newItem = Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_UI + "ScrollViewWeaponItem"), Vector3.zero, Quaternion.identity) as GameObject;
                _newItem.transform.SetParent(ScrollViewGrid.transform);
                _newItem.transform.localScale = Vector3.one;
                _newItem.GetComponent<ScrollViewWeaponItem>().InitItem(_Data);
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void InitSkillItems()
        {          
            foreach (WeaponData _Data in GameData.Instance.WeaponValues)
            {
                GameObject _newItem = Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_UI + "ScrollViewWeaponItem"), Vector3.zero, Quaternion.identity) as GameObject;
                _newItem.transform.SetParent(ScrollViewGrid.transform);
                _newItem.transform.localScale = Vector3.one;
                _newItem.GetComponent<ScrollViewWeaponItem>().InitItem(_Data);
            }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}