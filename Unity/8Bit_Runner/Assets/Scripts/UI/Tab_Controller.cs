﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class Tab_Controller : Singleton<Tab_Controller>
    {
        public enum TABSTATE
        {
            SKILLS,
            WEAPONS,
            OPTIONS
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public TABSTATE TabState
        {
            get { return m_TabState; }
            set {  m_TabState = value; SetActiveTab(m_TabState);  }
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        public GameObject SkillTab;
        public GameObject WeaponTab;
        public GameObject OptionsTab;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private GameObject m_activeTab;
        private TABSTATE m_TabState;
   
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void SwitchTabDisplay(GameObject _newTab)
        {

            if(m_activeTab!=null) m_activeTab.GetComponent<Tab>().SetActive(false);

            _newTab.SetActive(true);
            _newTab.GetComponent<Tab>().SetActive(true);
            m_activeTab = _newTab;
            
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void SetActiveTab(TABSTATE _Tab)
        {
            
            GameObject _newTab = null;
            switch (_Tab)
            {
                case TABSTATE.WEAPONS:
                    _newTab = WeaponTab;
                    break;
                case TABSTATE.SKILLS:
                    _newTab = SkillTab;
                    break;
                case TABSTATE.OPTIONS:
                    _newTab = OptionsTab;
                    break;
                default:
                    
                    _newTab = WeaponTab;
                    break;
            }
            SwitchTabDisplay(_newTab);
            RefreshScrollView();
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private void RefreshScrollView()
        {
            ScrollView_Controller.Instance.InitItems();
        }
    }
}