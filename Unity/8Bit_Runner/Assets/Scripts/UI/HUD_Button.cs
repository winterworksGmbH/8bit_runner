﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{
    public class HUD_Button : MonoBehaviour
    {

        public UI_Controller UIController;

        void Start()
        {
            HUD();
        }

        public void HUD()
        {         
            UIController.UIState = UI_Controller.UISTATES.HUD;
        }
        public void OpenSkills()
        {
            UIController.UIState = UI_Controller.UISTATES.OPTIONS;
            Tab_Controller.Instance.TabState = Tab_Controller.TABSTATE.SKILLS;
        }
        public void OpenWeapons()
        {
            UIController.UIState = UI_Controller.UISTATES.OPTIONS;
            Tab_Controller.Instance.TabState = Tab_Controller.TABSTATE.WEAPONS;
        }
        public void OpenShop()
        {
            UIController.UIState = UI_Controller.UISTATES.OPTIONS;
            Tab_Controller.Instance.TabState = Tab_Controller.TABSTATE.SKILLS;
        }
    }
}