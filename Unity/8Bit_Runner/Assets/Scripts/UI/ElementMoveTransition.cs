﻿using UnityEngine;
using DG.Tweening;
using System;
//----------------------------------------------------------------------------------------
namespace EightBitRunner
{
    public enum TransitionType
    {
        TRANSITION_IN,
        TRANSITION_OUT
    }

    public enum TransitionDirection
    {
        LEFT,
        RIGHT,
        TOP,
        BOTTOM
    }

    //----------------------------------------------------------------------------------------
    public class ElementMoveTransition : MonoBehaviour
    {
        private Vector3 m_originalPosition;
        private RectTransform m_panelRectTransform;
        private Vector3 m_transitionPosition;

       //----------------------------------------------------------------------------------------
       public void Transition(TransitionType _transitionType = TransitionType.TRANSITION_IN, TransitionDirection _transitionDirection = TransitionDirection.TOP, float _duration = 1f, Ease _ease= Ease.OutCubic, float _delay = 0f, Vector3 _overwritePosition =default(Vector3),Action _onStart = null, Action _callBack = null)
        {
            
            //DOTween.defaultTimeScaleIndependent = true;
            // reset Position to the one originally stored, and restart all tweens because they might have been cancelled before they were finished
            if (m_panelRectTransform == null)
            {
                m_panelRectTransform = gameObject.GetComponent<RectTransform>();
                m_originalPosition = m_panelRectTransform.localPosition;
            }

            m_panelRectTransform.localPosition = _overwritePosition == Vector3.zero ? m_originalPosition : _overwritePosition;

    
            // in which direction to we want to transition?
            switch (_transitionDirection)
            {
                case TransitionDirection.LEFT:
                    m_transitionPosition = new Vector3 ((Screen.width+m_panelRectTransform.sizeDelta.x) * -0.25f, m_panelRectTransform.localPosition.y,m_panelRectTransform.localPosition.z);
                    break;
                case TransitionDirection.RIGHT:
                    m_transitionPosition = new Vector3 ((Screen.width + m_panelRectTransform.sizeDelta.x)* 0.25f, m_panelRectTransform.localPosition.y,m_panelRectTransform.localPosition.z);
                    break;
                case TransitionDirection.TOP:
                    m_transitionPosition = new Vector3(m_panelRectTransform.localPosition.x, Screen.height + m_panelRectTransform.sizeDelta.y, m_panelRectTransform.localPosition.z);
                    break;
                case TransitionDirection.BOTTOM:
                    m_transitionPosition = new Vector3(m_panelRectTransform.localPosition.x, (Screen.height + m_panelRectTransform.sizeDelta.y)* -0.25f, m_panelRectTransform.localPosition.z);
                    break;
            }

            // do we want to transition in or out?
            switch (_transitionType)
            {
                case TransitionType.TRANSITION_IN:
                    m_panelRectTransform.DOAnchorPos3D(m_transitionPosition, _duration)
                        .From().SetEase(_ease)
                        .SetId("TransitionTween")
                        .SetDelay(_delay)
                        .SetUpdate(true)
                        .OnStart(()=>OnStart(_onStart))
                        .OnComplete(() => OnComplete(_callBack));
                    break;
                case TransitionType.TRANSITION_OUT:
                    m_panelRectTransform.DOAnchorPos3D(m_transitionPosition, _duration).SetEase(_ease).SetId("TransitionTween").SetDelay(_delay).SetUpdate(true).OnComplete(() => OnComplete(_callBack));
                    break;
            }
        }
        //----------------------------------------------------------------------------------------
        private void OnStart(Action _onStart)
        {
            if (_onStart != null)
            {
                _onStart();
            }
        }
        //----------------------------------------------------------------------------------------
        private void OnComplete(Action _callBack)
        {
            if (_callBack != null)
            {
                _callBack();
            }
        }
        //----------------------------------------------------------------------------------------
        private void OnDisable()
        {
            m_panelRectTransform.DOKill(true);
        }

    }
}