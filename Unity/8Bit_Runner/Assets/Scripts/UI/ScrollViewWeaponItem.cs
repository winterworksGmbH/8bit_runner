﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace EightBitRunner
{
    public class ScrollViewWeaponItem : MonoBehaviour
    {

        public Image Item_icon;
        public TextMeshProUGUI Name_txt;
        public TextMeshProUGUI MaxAmmo_txt;
        public TextMeshProUGUI Damage_txt;

        private Image m_Icon;
        private Sprite m_ammo;
        private Sprite m_damageIconIcon;
        private Sprite m_fireRateIcon;

        private int m_fireRate;
        private int m_damage;
        private int m_maxAmmo;

        WeaponTemplate m_WeaponTemplate;

        public void InitItem(WeaponData _data)
        {
            Name_txt.SetText(_data.Id);
            Item_icon.sprite = _data.Template.Icon;
            Item_icon.SetNativeSize();
            Damage_txt.SetText((_data.Damage*_data.BulletsPerShot).ToString());
            MaxAmmo_txt.SetText(_data.MaxAmmo.ToString());
        }

    }
}