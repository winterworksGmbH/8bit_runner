﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using DG.Tweening;
using UnityEngine.UI;

namespace EightBitRunner
{
    public class StartScreen_AnimationController : MonoBehaviour
    {
        public GameObject Logo;
        public GameObject Logo_bulletHole;
        public GameObject Flash;
        public GameObject ClickArea;

        private Action m_transitionEndedCallBack;


        public void TransitionToGame(Action _callback = null)
        {
            UI_AudioController.Instance.PlaySound(UI_AudioController.Instance.Explosion);
            ClickArea.SetActive(false);
            m_transitionEndedCallBack = _callback;
            Logo.SetActive(false);
            Logo_bulletHole.SetActive(true);
            UI_Controller.Instance.gameObject.GetComponent<Screenshake>().Shake(.05f,10f,1f,FadeOutScreen);
            GetComponent<CanvasGroup>().DOFade(0, 1).SetDelay(0.5f);
            Logo_bulletHole.transform.DOPunchScale(Vector3.one, 0.1f);
            Flash.gameObject.SetActive(true);
            Flash.GetComponent<Image>().DOFade(0, 0.5f);
        }

        
        private void FadeOutScreen()
        {
            if (m_transitionEndedCallBack != null)
            {
                m_transitionEndedCallBack();
            }
        }
       
    }
}