﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace EightBitRunner
{

    public class StartScreen_Buttons : MonoBehaviour
    {
        public UI_Controller UI_Controller;

        public void OnStartScreenTouch()
        {
            UI_Controller.UIState = UI_Controller.UISTATES.HUD;
        }

    }
}