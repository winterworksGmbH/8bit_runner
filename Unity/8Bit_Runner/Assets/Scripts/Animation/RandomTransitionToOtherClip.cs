﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomTransitionToOtherClip : StateMachineBehaviour {

    // can be added to animation state machines, randomly set transitionToOtherClip boolean true, so instead of looping it transitions to another clip

    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.SetBool("transitionToOtherClip", (Random.Range(0,10)>3?true:false));
    }


}
