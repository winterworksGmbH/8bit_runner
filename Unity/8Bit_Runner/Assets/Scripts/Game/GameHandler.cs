﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace EightBitRunner
{
    public class GameHandler : Singleton<GameHandler>
    {
      
        void Awake()
        {            
        }

        void Start()
        {
            LoadLevel("Street");
            GunGlow.Instance.GetLightsFromScene();
            UI_Controller.Instance.UIState = UI_Controller.UISTATES.START;
            // spawn test enemy
            //SpawnEnemy(new Vector3(-1.0f, 1.0f, 10.0f), GameData.Instance.EnemyValues[0]);
        }

        void Update()
        {
        }

        public void SpawnEnemy(Vector3 _position, EnemyData _data)
        {
            GameObject enemy = Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_ENEMY + "EnemyInstance"), _position, Quaternion.identity) as GameObject;
            enemy.GetComponent<Enemy>().Init(_data);
        }

        void LoadLevel(string _name)
        {
            Debug.Log("LoadLevel: " + _name);
            SceneManager.LoadScene(_name + "_01", LoadSceneMode.Additive);

            Debug.Log("sky");
            GameObject sky = Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_LEVEL + _name + "/Sky")) as GameObject;

            Debug.Log("lights");
            Instantiate(Resources.Load(GameConstants.Instance.RESOURCE_PATH_LEVEL + _name + "/Lights"));            
        }
    }    
}
