﻿// Singleton Template
using UnityEngine;
using UnityEngine.Assertions;

namespace EightBitRunner
{
    public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {        
        private static T m_instance = null;
        
        public static T Instance
        {
            get { return getInstance(); }
        }

        private static T getInstance()
        {
            // not created yet? then create it
            if (m_instance == null)
            {
                string name = typeof(T).ToString();

                // get active objects<T>
                T[] objects = FindObjectsOfType<T>();
                
                // found=
                if (objects.Length > 0)
                {
                    // there can be only one!
                    Assert.AreEqual(1, objects.Length, "Found more than one instance of " + name + "! Singleton is broken!");
                    m_instance = objects[0];
                }

                // not found? then create
                if (m_instance == null)
                    m_instance = new GameObject(name).AddComponent<T>();
            }

            return m_instance;
        }
    }
}