﻿// helper class to get fps camera

using UnityEngine;

namespace EightBitRunner
{
    class CameraHelper
    {
        public static Camera FPSCamera
        {
            get { return GameObject.FindWithTag(Tags.FPS_CAMERA).GetComponent<Camera>(); }
        }
    }
}