﻿using UnityEngine;

namespace EightBitRunner
{
	public class CameraMovement : MonoBehaviour
	{
        private const int layerMask = 1 << (int)LayerMasks.WALLS; // filter out everything except walls
        private float m_head_movement_offset = 0.0f;
        private Vector3 m_position = new Vector3(0.0f, 0.0f, 0.0f);

        private Camera m_camera;

        // collision with walls
        private Vector3 capsule_point_back;
        private Vector3 capsule_point_front;

        void Start ()
		{
            m_head_movement_offset = GameConstants.Instance.CAMERA_HEAD_POSITION_Y;

            m_camera = GetComponent<Camera>();
            m_camera.transparencySortMode = TransparencySortMode.Orthographic;
        }
		
		void Update ()
		{
            DoHeadMovement();
			CalcPositionBetweenWalls ();
		}

        private void DoHeadMovement()
        {
            // inc head movement offset
            m_head_movement_offset += GameConstants.Instance.CAMERA_HEAD_MOVEMENT_SPEED * Time.deltaTime;

            // calc offset in unity units
            float head_offset = Mathf.Abs(Mathf.Sin(m_head_movement_offset) * GameConstants.Instance.CAMERA_HEAD_MOVEMENT_FACTOR);

            // apply to camera y position
            m_position.x = m_camera.transform.position.x;
            m_position.y = GameConstants.Instance.CAMERA_HEAD_POSITION_Y + head_offset;
            m_position.z = m_camera.transform.position.z;
            m_camera.transform.position = m_position;
        }


        // check left and right wall and position the camera in between
        private void CalcPositionBetweenWalls()
		{
            // setup points for capsule
            capsule_point_back.x = m_camera.transform.position.x;
            capsule_point_back.y = m_camera.transform.position.y;
            capsule_point_back.z = m_camera.transform.position.z;

            capsule_point_front.x = m_camera.transform.position.x;
            capsule_point_front.y = m_camera.transform.position.y;
            capsule_point_front.z = m_camera.transform.position.z + GameConstants.Instance.CAMERA_COLLISION_CAPSULE_LENGTH;

			// do capsule cast collision check
			RaycastHit hit_left;
			RaycastHit hit_right;            
			Physics.CapsuleCast (capsule_point_back, capsule_point_front, GameConstants.Instance.CAMERA_COLLISION_CAPSULE_RADIUS, Vector3.left, out hit_left, GameConstants.Instance.CAMERA_COLLISION_RAY_LENGTH, layerMask);
			Physics.CapsuleCast (capsule_point_back, capsule_point_front, GameConstants.Instance.CAMERA_COLLISION_CAPSULE_RADIUS, Vector3.right, out hit_right, GameConstants.Instance.CAMERA_COLLISION_RAY_LENGTH, layerMask);
				
			// calc center between two hits
			float hit_center_x = hit_left.point.x + (hit_right.point.x - hit_left.point.x) * 0.5f;
            float new_x = m_camera.transform.position.x + (hit_center_x - m_camera.transform.position.x) * 0.025f;

            // move towards new center
            m_position.x = new_x;
            m_position.y = m_camera.transform.position.y;
            m_position.z = m_camera.transform.position.z + GameConstants.Instance.CAMERA_MOVEMENT_SPEED * Time.deltaTime;

            m_camera.transform.position = m_position;
		}
	}
}
