﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

namespace EightBitRunner
{
    [CustomEditor(typeof(Screenshake))]
    public class ScreenshakeEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            var targetScript = target as Screenshake;

            GUILayout.Label("Shaking the UI needs some transforms", EditorStyles.boldLabel);
            targetScript.UIShake = EditorGUILayout.BeginToggleGroup("Shake the UI/Ass?", targetScript.UIShake);
            SerializedProperty tps = serializedObject.FindProperty("ObjectsToShake");
            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(tps, true);
            if (EditorGUI.EndChangeCheck())
                serializedObject.ApplyModifiedProperties();
            EditorGUILayout.EndToggleGroup();

        }
    }


    public class Screenshake : MonoBehaviour
    {
        public bool UIShake;
        public Transform[] ObjectsToShake;

        private Vector3 m_originalCameraPosition;

        private List<Transform> m_ObjectsToShake;
        private List<Vector3> m_OriginalPositions;

        private float m_shakeAmt = 0;
        
        private Camera m_Camera;
        private Action m_callBack = null;

        void Start()
        {
            if (!UIShake)
                m_Camera = GetComponent<Camera>();
        }

        public void Shake(float _frequency, float _amplitude, float _duration, Action _callBack = null)
        {
            m_ObjectsToShake = new List<Transform>();
            m_OriginalPositions = new List<Vector3>();
            m_callBack = _callBack;

            foreach (Transform _tr in ObjectsToShake)
            {
                m_ObjectsToShake.Add(_tr);
                m_OriginalPositions.Add(_tr.transform.localPosition);
            }

            m_shakeAmt = _amplitude;
            InvokeRepeating("CameraShake", 0, _frequency);
            Invoke("StopShaking", _duration);
        }

        private void CameraShake()
        {
            if (m_shakeAmt > 0)
            {
                float _quakeAmt = UnityEngine.Random.Range(-1,1) * m_shakeAmt;
                foreach (Transform _tr in m_ObjectsToShake)
                {
                    Vector3 _pos = _tr.position;
                    _pos.y = _quakeAmt / 2;
                    _pos.x = _quakeAmt;
                   
                    _tr.localPosition = _pos;
                }

            }
        }

        private void StopShaking()
        {
            CancelInvoke("CameraShake");

            foreach (Transform _tr in m_ObjectsToShake)
            {
                _tr.transform.position = m_OriginalPositions[m_ObjectsToShake.FindIndex(i => i == _tr)];
            }
            if (m_callBack != null)
            {
                m_callBack();
            }
        }


    }
}