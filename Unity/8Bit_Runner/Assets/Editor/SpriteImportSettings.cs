﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Text.RegularExpressions;

public class SpriteImportSettings : AssetPostprocessor
{

    void OnPreprocessTexture()
    {
        
        List<string> _assetFolders = new List<string>(Regex.Split(assetPath, @"(?<=[/])"));
        _assetFolders.RemoveAt(_assetFolders.Count - 1);
        string _pathOfAsset = String.Join(String.Empty, _assetFolders.ToArray());


        if (_assetFolders.Contains("UI/"))
        {
            TextureImporter _textureImporter = (TextureImporter)assetImporter;
            _textureImporter.spritePixelsPerUnit = 100;
            _textureImporter.filterMode = FilterMode.Point;
            _textureImporter.textureType = TextureImporterType.Sprite;
            _textureImporter.textureCompression = TextureImporterCompression.Uncompressed;
     

            string _lastFolder = _assetFolders[_assetFolders.Count - 1];
            switch (_lastFolder)
            {
                case ("BG/"):
                    _textureImporter.spritePackingTag = "UI_BG";
                    break;
                case ("Buttons/"):
                    _textureImporter.spritePackingTag = "UI_Buttons";
                    break;
                case ("Icons/"):
                    _textureImporter.spritePackingTag = "UI_Icons";
                    break;
                case ("Panels/"):
                    _textureImporter.spritePackingTag = "UI_Panels";
                    break;
                case ("Weapons/"):
                    _textureImporter.spritePackingTag = "UI_Weapons";
                    break;
                default:
                    _textureImporter.spritePackingTag = "UI";
                    break;
            }

        }
        
    }


}
