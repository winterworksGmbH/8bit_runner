﻿// helper functions for shortcut menu

using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System;
using UnityEditor;

namespace EightBitRunner
{

    public static class ShortcutsHelper
    {
        private static Hashtable m_tablenameToTypeLUT = new Hashtable();
        private static Hashtable m_tablenameToInspectorListLUT = new Hashtable();

        static ShortcutsHelper()
        {
            m_tablenameToTypeLUT.Add("weapons", typeof(WeaponData));
            m_tablenameToTypeLUT.Add("enemies", typeof(EnemyData));
            m_tablenameToInspectorListLUT.Add("weapons", GameData.Instance.WeaponValues);
            m_tablenameToInspectorListLUT.Add("enemies", GameData.Instance.EnemyValues);
        }

        // parse JSON data from google spread sheets into instances of data classes like WeaponData etc.
        public static void ParseData(CloudConnectorCore.QueryType query, List<string> objTypeNames, List<string> jsonData)
        {
            // workaround for making sure the GameData singleton exists in the editor
            GameData.Instance.ToString();

            // table received
            if (query == CloudConnectorCore.QueryType.getAllTables)
            {
                string tableName;
                for (int i = 0; i < objTypeNames.Count; i++)
                {
                    tableName = objTypeNames[i];

                    // nasty ugly dirty hack! do this right ASAP!
                    if (tableName == "weapons")
                    {
                        // get type for objects in table
                        Type type = (Type)m_tablenameToTypeLUT[tableName];

                        // convert objects from jason
                        // TODO: we need dynamic types here once there is more info than WeaponData!
                        WeaponData[] entries = GSFUJsonHelper.JsonArray<WeaponData>(jsonData[i]);

                        // get list, clear it and add entries
                        List<WeaponData> list = (List<WeaponData>)m_tablenameToInspectorListLUT[tableName];
                        list.Clear();

                        WeaponData data;
                        for (int j = 0; j < entries.Length; j++)
                        {
                            data = entries[j];
                            data.Template = Resources.Load(GameConstants.Instance.RESOURCE_PATH_WEAPON_TEMPLATES + data.TemplateName) as WeaponTemplate;
                            list.Add(data);
                        }
                    }
                    else if (tableName == "enemies")
                    {
                        // get type for objects in table
                        Type type = (Type)m_tablenameToTypeLUT[tableName];

                        // convert objects from jason
                        // TODO: we need dynamic types here once there is more info than WeaponData!
                        EnemyData[] entries = GSFUJsonHelper.JsonArray<EnemyData>(jsonData[i]);

                        // get list, clear it and add entries
                        List<EnemyData> list = (List<EnemyData>)m_tablenameToInspectorListLUT[tableName];
                        list.Clear();

                        EnemyData data;
                        for (int j = 0; j < entries.Length; j++)
                        {
                            data = entries[j];
                            data.Template = Resources.Load(GameConstants.Instance.RESOURCE_PATH_ENEMY_TEMPLATES + data.TemplateName) as EnemyTemplate;
                            list.Add(data);
                        }
                    }
                }

                EditorUtility.DisplayDialog("Get Game Data", "Ich habe fertig!", "Fein!");
            }            
        }
    }
}
