﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace EightBitRunnerTools
{
    public class FloorNavMeshHelper : EditorWindow
    {

        [MenuItem("8Bit_Runner/Tools/FloorNavMeshHelper")]
        static void Init()
        {
            FloorNavMeshHelper window = (FloorNavMeshHelper)EditorWindow.GetWindow(typeof(FloorNavMeshHelper));
            window.Show();
        }

        void OnGUI()
        {
            GUILayout.Label("Select floor tiles to add to navmesh static", EditorStyles.boldLabel);

            if (GUILayout.Button("Select Floor Objects with Walkable Tag"))
            {
                GameObject[] _objectsWithWalkableTag = GameObject.FindGameObjectsWithTag("Walkable");
                List<GameObject> _children = new List<GameObject>();
                foreach (GameObject _go in _objectsWithWalkableTag)
                {

                    int _childCount = _go.transform.childCount;
                    for(int i = 0; i < _childCount; i++)
                    {
                        if (_go.transform.GetChild(i).gameObject.GetComponent<MeshRenderer>() != null)
                        {
                            _children.Add(_go.transform.GetChild(i).gameObject);
                        }
                    }
                }
                Selection.objects = _children.ToArray();
            }
            if(GUILayout.Button("Select with Raycast"))
            {
               
                
                List<GameObject> _floors = new List<GameObject>();
                List<GameObject> _walkableFloorParents = new List<GameObject>();
                List<GameObject> _walkableFloors = new List<GameObject>();
                foreach (GameObject _go in FindObjectsOfType<GameObject>())
                {

                    if (_go.layer == 8)
                    {
                        
                        _floors.Add(_go);
                    }
                }
               foreach(GameObject _floor in _floors)
                {
                    Debug.Log("juhu");
                    RaycastHit _hit;
                    if(Physics.Raycast(_floor.transform.position,Vector3.up,out _hit))
                    {

                    }
                    else
                    {
                        _walkableFloorParents.Add(_floor);
                    }
                }
                foreach(GameObject _walkableFloor in _walkableFloorParents)
                {
                    int _childCount = _walkableFloor.transform.childCount;
                    for(int i = 0; i < _childCount; i++)
                    {
                        if (_walkableFloor.transform.GetChild(i).gameObject.GetComponent<MeshRenderer>() != null)
                        {
                            _walkableFloors.Add(_walkableFloor.transform.GetChild(i).gameObject);
                        }
                    }
                }
                Selection.objects = _walkableFloors.ToArray();
            }
        }
    }
}