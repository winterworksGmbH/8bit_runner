﻿using UnityEditor.SceneManagement;
using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using System.Collections;
using EightBitRunner;

namespace EightBitRunnerTools
{
    public class Shortcuts : Editor
    {
        private static readonly string GAME_SCENE = "Assets/Scenes/Game.unity";
        private static bool m_listenerAttached = false;
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 
        [MenuItem("8Bit_Runner/Open Game Scene")]
        private static void OpenGameScene()
        {
            if (SceneManager.GetActiveScene().name != GAME_SCENE)
            {
                EditorSceneManager.OpenScene(GAME_SCENE);
            }
        }
        [MenuItem("8Bit_Runner/JETZT ZOCKERN!")]
        private static void OpenGameAndPlay()
        {
            if (SceneManager.GetActiveScene().name != GAME_SCENE)
            {
                EditorSceneManager.SaveScene(SceneManager.GetActiveScene());
            }
            EditorSceneManager.OpenScene(GAME_SCENE);
            EditorApplication.isPlaying = true;
        }
        [MenuItem("8Bit_Runner/Get Game Data")]
        private static void GetGameData()
        {
            // attach listener if necessary
            if (!m_listenerAttached)
            {
                CloudConnectorCore.processedResponseCallback.AddListener(ShortcutsHelper.ParseData);
                m_listenerAttached = true;
            }

            // get all weapons
            CloudConnectorCore.GetAllTables(false);
            //CloudConnectorCore.GetTable("weapons", false);
            //CloudConnectorCore.GetTable("enemies", false);
        }        
    }

}