﻿using UnityEditor;
using UnityEngine;

namespace EightBitRunner
{
/*
    [CustomPropertyDrawer(typeof(WeaponData))]
    public class WeaponDataPropertyDrawer : PropertyDrawer
    {
        // draw the property inside the given rect
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            // override complete property
            EditorGUI.BeginProperty(position, label, property);

            // draw label
            position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Passive), label);

            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // calc rects
            var idRect = new Rect(position.x, position.y, 95, position.height);
            var bulletTypeRect = new Rect(position.x + 100, position.y, 50, position.height);
            var bulletsPerShotRect = new Rect(position.x + 160, position.y, 50, position.height);

            // draw fields
            //EditorGUI.PropertyField(idRect, property.FindPropertyRelative("Id"), GUIContent.none);
            EditorGUI.PropertyField(bulletTypeRect, property.FindPropertyRelative("BulletType"), GUIContent.none);
            EditorGUI.PropertyField(bulletsPerShotRect, property.FindPropertyRelative("BulletsPerShot"), GUIContent.none);

            //        public int BulletDelay;
            //public int CooldownTime;
            //public int Clipsize;
            //public int ReloadTime;

            // restore indent
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
    */
}