﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace EigthBitRunnerTools
{
    public class LevelSetBuilder : EditorWindow
    {

        private bool groupEnabled;

        private Transform m_blockTemplate;
        private List<Transform> m_blockTemplates = new List<Transform>();
        private int m_blocksInRow;

        private string m_newSetName;
        private Material m_newSetMaterial;

        private string m_newSetRootPath;
        private string m_defaultRootPath = "Assets/Prefabs/Level/";
        private string m_newSetPath;

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        [MenuItem("8Bit_Runner/Tools/CreateLevelSetPrefabs")]
        static void Init()
        {
            LevelSetBuilder window = (LevelSetBuilder)EditorWindow.GetWindow(typeof(LevelSetBuilder));
            window.Show();
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        void OnGUI()
        {
            GUILayout.Label("Base Settings", EditorStyles.boldLabel);

            m_blockTemplate = (Transform)EditorGUILayout.ObjectField("Root BlockTemplate", m_blockTemplate, typeof(Transform), true);
            m_newSetName = EditorGUILayout.TextField("New SetName", m_newSetName);
            m_newSetMaterial = (Material)EditorGUILayout.ObjectField("Material", m_newSetMaterial, typeof(Material), false);
            
            groupEnabled = EditorGUILayout.BeginToggleGroup("Change Default Folder", groupEnabled);

            m_newSetRootPath = EditorGUILayout.TextField("Create Set in...", m_defaultRootPath);
            EditorGUILayout.EndToggleGroup();
           

            if (GUILayout.Button("Und ab geht die Lutzi!"))
            {
                m_newSetPath = m_newSetRootPath + m_newSetName;
                if (CheckForDirectory())
                {

                }
                else
                {
                    Debug.LogWarning("Directory does allready exist" + m_newSetPath);
                }
                if (m_blockTemplate && m_newSetMaterial && m_blockTemplate.gameObject.scene.IsValid())
                {
                    CreateLevelSetPrefabs(m_blockTemplate);
                    // Todo
                    //CreateFolders();
                }
                else
                {
                    Debug.LogWarning("Missing Object in Scene,missing Material or no name entered");
                }
            }

        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void CreateLevelSetPrefabs(Transform _blockTemplate)
        {
            m_blockTemplates.Clear();
            foreach (Transform tr in _blockTemplate)
            {
               m_blockTemplates.Add(tr);
            }
            m_blockTemplates.ForEach(x => CreateBlockParent(x.gameObject));
            if (Mathf.Sqrt(m_blockTemplates.Count) % 2 == 0)
            {
                // todo
            }
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void CreateBlockParent(GameObject _block)
        {         
            GameObject _blockParent = new GameObject(_block.name);
            _blockParent.transform.position = _block.transform.position;
            _block.transform.parent = _blockParent.transform;
            _block.GetComponent<Renderer>().material = m_newSetMaterial;
            _blockParent.name = CreateNewName(_block);
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        private bool CheckForDirectory()
        {
            bool _exists = false;
            return _exists;
        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        private void CreateFolders()
        {
            string _newPath = m_newSetRootPath + m_newSetName;
            Debug.Log(_newPath);
            if (!Directory.Exists(_newPath)){
                Directory.CreateDirectory(_newPath);
                Directory.CreateDirectory(_newPath + "/floors");
                Directory.CreateDirectory(_newPath + "/ceilings");
                Directory.CreateDirectory(_newPath + "/walls");
                AssetDatabase.Refresh();
            }
            else
            {
                Debug.LogWarning("Folder does allready exist!");
            }
            
        }

        private string CreateNewName(GameObject _block)
        {
            if (Mathf.Sqrt(m_blockTemplates.Count) % 2 == 0)
            {
                Debug.Log("yooo! enough tiles!!!");
            }
            string _newName = _block.name.Replace("Block_Template_",m_newSetName);
            return _newName;
        }
        private void CreatePrefab(GameObject _block)
        {
            
          //  Object prefab = EditorUtility.CreateEmptyPrefab(m_newSetRootPath+_block.name)
        }

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    }
}